class Nav extends React.Component{
    constructor(props) {
        super(props);
    }
    render(){
        return(
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item active">
                            <a className="nav-link" href="/mobile">Home <span className="sr-only">(current)</span></a>
                        </li>
                    </ul>
                </div>
            </nav>
        );
    }
}

ReactDOM.render(
    <Nav />,
    document.getElementById('nav')
);
