<?php

namespace App\Controller;

class MobileController extends AppController
{
    public $components = array('RequestHandler');
    public function initialize()
    {
        $this->loadModel('Mobiles');
    }

    public function index()
    {
        $mobile = $this->Mobiles->find('all');
        $this->set(['mobile' => $mobile, '_serialize' => 'mobile']);
    }

    public function view($id)
    {
        $mobile = $this->Mobiles->get($id);
        $this->set(['mobile' => $mobile, '_serialize' => 'mobile']);
    }

    public function add()
    {
        $mobile = $this->Mobiles->newEntity($this->request->getData());
        if ($this->Mobiles->save($mobile)) {
            return $this->redirect(['action' => 'index']);
        }
        $this->set('mobile', $mobile);
    }

    public function edit($id)
    {
        $mobile = $this->Mobiles->get($id);
        $this->Mobiles->patchEntity($mobile, $this->request->getData());
        if ($this->request->is(['post', 'put'])) {
            if ($this->Mobiles->save($mobile)) {
                return $this->redirect(['action' => 'index']);
            }
        }
        $this->set('mobile', $mobile);
    }

    public function delete($id)
    {
        $mobile = $this->Mobiles->get($id);
        if (!$this->Mobiles->delete($mobile)) {
        }
        return $this->redirect(['action' => 'index']);
    }

    public function search()
    {
        $search = $this->request->getQuery('keyword');
        $this->set('mobiles', $this->Mobiles->find()->where(function ($exp, $query)use($search){
            return $exp->like('name_mobile','%'.$search.'%');
        }));
    }
}
