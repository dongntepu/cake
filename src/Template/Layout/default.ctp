<?php
$cakeDescription = '';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->css('bootstrap.min') ?>
    <?= $this->Html->script('react.development.js') ?>
    <?= $this->Html->script('react-dom.development.js') ?>
    <?= $this->Html->script('https://unpkg.com/axios/dist/axios.min.js') ?>

    <?= $this->Html->script('jquery-3.3.1.slim.min',['block'=>'scriptBottom']) ?>
    <?= $this->Html->script('popper.min',['block'=>'scriptBottom']) ?>


    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
</head>
<body>
    <div id="nav"></div>
    <?= $this->Flash->render() ?>
    <div class="container clearfix">
        <?= $this->fetch('content') ?>
    </div>
    <?= $this->fetch('scriptBottom') ?>
    <?= $this->Html->script('bootstrap.min') ?>
    <?= $this->Html->script('babel.min.js') ?>
</body>
<?= $this->Html->script('nav',['type'=>'text/babel']) ?>

</html>
