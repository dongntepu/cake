class Index extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            mobiles: [],
        }
    }

    componentDidMount() {
        axios.get('http://localhost:8010/mobile.json')
            .then(res => {
                const data = res.data;
                this.setState({ mobiles:data });
            })
    }

    handleDelete(id){
        if (confirm("Are you sure?")){
            axios.delete(`http://localhost:8010/mobile/${id}.json`)
            const mobiles = this.state.mobiles.filter(mobiles => mobiles.id !== id);
            this.setState({ mobiles });
        }
    }

    render(){
        return(
            <div>
                <div className="container clearfix">
                    <div className="row">
                        <div className="col-sm-12" id="mobile">
                            <h1 className="display-3">Mobile</h1>
                            <div>
                                <li className="navbar"><a href="/mobile/add" className="btn btn-primary">New Mobile</a></li>
                                <li className="search-navbar navbar navbar-light bg-light">
                                    <form className="form-inline" action="/mobile/search" method="get">
                                        <input name="keyword" className="form-control mr-sm-2" type="search" placeholder="Search" />
                                    </form>
                                </li>
                            </div>
                            <table className="table table-striped">
                                <thead>
                                <tr>
                                    <td>ID</td>
                                    <td>NAME</td>
                                    <td>PRICE</td>
                                    <td colSpan={2}>ACTIONS</td>
                                </tr>
                                </thead>
                                <tbody>
                                {this.state.mobiles.map((mobiles) => (
                                    <tr key={mobiles.id}>
                                        <td>{mobiles.id}</td>
                                        <td>{mobiles.name_mobile}</td>
                                        <td>{mobiles.price}</td>
                                        <td>
                                            <a href={'/mobile/' + mobiles.id} className="btn btn-primary">View</a>
                                        </td>
                                        <td>
                                            <a href={'/mobile/edit/' + mobiles.id} className="btn btn-primary">Edit</a>
                                        </td>
                                        <td>
                                            {/*<a href={'/mobile/delete/' + mobiles.id} className="btn btn-danger">Delete</a>*/}
                                            <button onClick={()=>this.handleDelete(mobiles.id)} className="btn btn-danger">Delete</button>
                                        </td>
                                    </tr>
                                ))}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

ReactDOM.render(
    <Index />,
    document.getElementById('index')
);
