// import Nav from '../nav';
class Edit extends React.Component{
    constructor(props) {
        super(props);
    }
    render(){
        return(
            <div className="row">
                <div className="col-sm-8 offset-sm-2">
                    <h1 className="display-3">Edit a Mobile</h1>
                    <div>
                        <div className="form-group">
                            <label htmlFor="name_mobile">Name:</label>
                            <input type="text" name="name_mobile" className="form-control" id="name_mobile" defaultValue={name} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="price">Price:</label>
                            <input type="text" name="price" className="form-control" id="price" defaultValue={price} />
                        </div>
                        <button className="btn btn-primary" type="submit">Edit Mobile</button>
                    </div>
                </div>
            </div>
        );
    }
}

var element = document.getElementById('edit');
if (element) {
    var name = element.dataset.name;
    var price = element.dataset.price;
    ReactDOM.render(
        <Edit/>,
        document.getElementById('edit')
    );
}

