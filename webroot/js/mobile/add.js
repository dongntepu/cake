class Add extends React.Component {

    render() {
        return (
            <div className="row">
                <div className="col-sm-8 offset-sm-2">
                    <h1 className="display-3">Add a Mobile</h1>
                    <div>
                        <div className="form-group">
                            <label htmlFor="name-mobile">Name Mobile</label>
                            <input type="text" name="name_mobile" className="form-control" id="name-mobile"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="price">Price</label>
                            <input type="text" name="price" className="form-control" id="price"/>
                        </div>
                        <button className="btn btn-primary" type="submit">Add Mobile</button>
                    </div>
                </div>
            </div>
        );
    }
}

ReactDOM.render(
    <Add/>,
    document.getElementById('add')
);
