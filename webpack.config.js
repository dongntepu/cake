// module.exports = {
//     entry: './src/app.js',
//     output: {
//         path: path.join(_dirname, 'public'),
//         filename: 'bundle.js',
//         module: {
//             rules: [
//                 {
//                     test: /\.(js|jsx)$/,
//                     exclude: /node_modules/,
//                     use: {
//                         loader: 'babel-loader',
//                         options: {
//                             presets: [
//                                 '@babel/preset-env',
//                                 '@babel/preset-react'
//                             ]
//                         }
//                     }
//                 }
//             ]
//         }
//     }
// }

const path = require("path");

module.exports = {
    entry: "./webroot/js/mobile/index.js",
    output: {
        path: path.join(__dirname, "/webroot/js"),
        filename: "index_bundle.js"
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                },
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"]
            }
        ]
    }
};
