class View extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return(
            <div className="row">
                <div className="col-sm-12">
                    <h1 className="display-3">Mobile</h1>
                    This Mobile has a name which is: {name} and the price is: {price}
                </div>
            </div>
        );
    }
}

var element = document.getElementById('view');
if (element)
{
    var name = element.getAttribute('name');
    var price = element.getAttribute('price');
    ReactDOM.render(
        <View />,
        document.getElementById('view')
    );

}
