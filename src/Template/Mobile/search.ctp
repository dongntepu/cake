<div class="row">
    <div class="col-sm-12">
        <h1 class="display-3">Mobile</h1>
        <div>
            <li class="navbar"><?php echo $this->Html->link('New Mobile',['action'=>'add'],['class'=>'btn btn-primary']);?></li>
            <li class="search-navbar navbar navbar-light bg-light">
                <form class="form-inline" action="<?php echo $this->Url->build(['action'=>'search'])?>" method="get">
                    <!--                    --><?php //echo $this->Form->input('',['action'=>'search'],['class'=>'form-control mr-sm-2'],['type'=>'get']);?>
                    <input name="keyword" class="form-control mr-sm-2" type="search" placeholder="Search">
                </form>
            </li>
        </div>
        <table class="table table-striped">
            <thead>
            <tr>
                <td>ID</td>
                <td>NAME</td>
                <td>PRICE</td>
                <td colspan = 2>ACTIONS</td>
            </tr>
            </thead>
            <tbody>
            <?php foreach($mobiles as $mobile):?>
                <tr id="<?php echo $mobile->id;?>">
                    <td><?php echo $mobile->id;?></td>
                    <td><?php echo $mobile->name_mobile;?></td>
                    <td><?php echo $mobile->price;?></td>
                    <td>
                        <?php echo $this->Html->link('View',['action'=>'view', $mobile->id],['class'=>'btn btn-primary']);?>
                    </td>
                    <td>
                        <?php echo $this->Html->link('Edit',['action'=>'edit', $mobile->id],['class'=>'btn btn-primary']);?>
                    </td>
                    <td>
                        <?php echo $this->Form->postLink('Delete',['action'=>'delete', $mobile->id],['class'=>'btn btn-danger']);?>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>
